package com.mk;

public class Deluxe extends Hamburger {
    //no extra additions but you can choose chips and drinks
    private boolean chips;
    private String drinks;
    private int toCompare;

    public Deluxe(String bread, String meat, boolean chips, String drinks) {
        super(bread, meat);
        this.toCompare = 0;
        if(chips) {
            this.chips = chips;
            setNo(getNo() + 1);
            this.toCompare +=1;
            setPrice(getPrice() + 1.5);
        }
        this.drinks = chooseDrinks(drinks);
    }

    public String chooseDrinks(String drinks) {
        if(drinks == "Mirinda") {
            setNo(getNo() + 1);
            setPrice(getPrice() + 2);
            this.toCompare +=1;
            return "Mirinda";
        }
        else if (drinks == "Pepsi") {
            setNo(getNo() + 1);
            setPrice(getPrice() + 2);
            this.toCompare +=1;
            return "Pepsi";
        }
        return null;
    }

    @Override
    public void printBurger() {
        if(this.toCompare == 0)
            super.printBurger();
        else if(this.toCompare == 1) {
            if( getNo() == 1 ) System.out.println("And no additions. ");
            if( getNo() == 2 ) System.out.println("And one addition: " + getAddon2());
            if( getNo() == 3 ) System.out.println("And two additions: " + getAddon2() + ", " + getAddon3());
            if( getNo() == 4 ) System.out.println("And three additions: " + getAddon2() + ", " + getAddon3() + ", " + getAddon4());
        }
        else if(this.toCompare == 2) {
            if( getNo() == 2 ) System.out.println("And no additions. ");
            if( getNo() == 3 ) System.out.println("And one addition: " + getAddon3());
            if( getNo() == 4 ) System.out.println("And two additions: " + getAddon3() + ", " + getAddon4());
        }
        if(this.drinks != null) {
            System.out.println("Additional drink is: " + this.drinks);
        }
        if(this.chips)
            System.out.println("You added chips. ");

        System.out.println("Price: $" + getPrice());
    }
}
