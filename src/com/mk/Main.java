package com.mk;

public class Main {

    public static void main(String[] args) {

        Hamburger myBurger = new Hamburger("white", "pork");
        myBurger.printBurger();
        System.out.println("\n");

        myBurger.addCheese();
        myBurger.addOnion();
        myBurger.addTomato();
        myBurger.printBurger();
        System.out.println("\n");

        Healthy myHealthy = new Healthy("pork");
        myHealthy.addCheese();
        myHealthy.addLettuce();
        myHealthy.addOnion();
        myHealthy.addTomato();
        myHealthy.addLettuce();
        myHealthy.addLettuce();
        myHealthy.addLettuce();
        myHealthy.printBurger();
        System.out.println("\n");

        Deluxe newDeluxe = new Deluxe("white", "pork", true, "Pepsi");
        newDeluxe.addCheese();
        newDeluxe.addOnion();
        newDeluxe.printBurger();

    }
}
