package com.mk;

public class Healthy extends Hamburger{
    private String addon5;
    private String addon6;

    public Healthy(String meat) {
        super("brown", meat);
    }

    @Override
    public int isFull() {
        if(getNo() == 6)
            return 0;
        return 1;
    }

    @Override
    public int addLettuce() {
        if(isFull() == 0) {
            System.out.println("No more addons! ");
            return 0;
        }
        switch(getNo()) {
            case 4: this.addon5 = "Lettuce"; setNo(getNo() + 1); setPrice(getPrice() + 0.5); return 1;
            case 5: this.addon6 = "Lettuce"; setNo(getNo() + 1); setPrice(getPrice() + 0.5); return 1;
        }
        super.addLettuce();
        return 1;
    }

    @Override
    public int addTomato() {
        if(isFull() == 0) {
            System.out.println("No more addons! ");
            return 0;
        }
        switch(getNo()) {
            case 4: this.addon5 = "Tomato"; setNo(getNo() + 1); setPrice(getPrice() + 0.5); return 1;
            case 5: this.addon6 = "Tomato"; setNo(getNo() + 1); setPrice(getPrice() + 0.5); return 1;
        }
        super.addTomato();
        return 1;
    }

    @Override
    public int addCheese() {
        if(isFull() == 0) {
            System.out.println("No more addons! ");
            return 0;
        }
        switch(getNo()) {
            case 4: this.addon5 = "Cheese"; setNo(getNo() + 1); setPrice(getPrice() + 1); return 1;
            case 5: this.addon6 = "Cheese"; setNo(getNo() + 1); setPrice(getPrice() + 1); return 1;
        }
        super.addCheese();
        return 1;
    }

    @Override
    public int addOnion() {
        if(isFull() == 0) {
            System.out.println("No more addons! ");
            return 0;
        }
        switch(getNo()) {
            case 4: this.addon5 = "Onion"; setNo(getNo() + 1); setPrice(getPrice() + 0.3); return 1;
            case 5: this.addon6 = "Onion"; setNo(getNo() + 1); setPrice(getPrice() + 0.3); return 1;
        }
        super.addOnion();
        return 1;
    }

    @Override
    public void printBurger() {
        super.printBurger();
        if( getNo() == 5 ) System.out.println("And five additions: " + getAddon1() + ", " + getAddon2() + ", "
                + getAddon3() + ", " + getAddon4() + ", " + this.addon5);
        if( getNo() == 6 ) System.out.println("And six additions: " + getAddon1() + ", " + getAddon2() + ", "
                + getAddon3() + ", " + getAddon4() + ", " + this.addon5 + ", " + this.addon6);
    }
}
