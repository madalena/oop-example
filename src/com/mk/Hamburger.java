package com.mk;

public class Hamburger {
    private String bread;
    private String meat;
    private String addon1;
    private String addon2;
    private String addon3;
    private String addon4;
    private double price;
    private int no;

    public Hamburger(String bread, String meat) {
        this.bread = chooseBreadRoll(bread);
        this.meat = meat;
        this.price = 4; //startowa cena za bułkę i mięso
        this.no = 0;
    }

    public int isFull() {
        if(this.no == 4)
            return 0;
        return 1;
    }

    private String chooseBreadRoll(String bread) {
        if(bread == "brown")
            return "brown";
        else if (bread == "riceRoll")
            return "riceRoll";
        else
            return "white";
    }

    public int addLettuce() {
        if(isFull() == 0) {
            System.out.println("No more addons! ");
            return 0;
        }
        switch(this.no) {
            case 0: this.addon1 = "Lettuce"; this. no += 1; this.price += 0.5; break;
            case 1: this.addon2 = "Lettuce"; this. no += 1; this.price += 0.5; break;
            case 2: this.addon3 = "Lettuce"; this. no += 1; this.price += 0.5; break;
            case 3: this.addon4 = "Lettuce"; this. no += 1; this.price += 0.5; break;
        }
        return 1;
    }

    public int addTomato() {
        if(isFull() == 0) {
            System.out.println("No more addons! ");
            return 0;
        }
        switch(this.no) {
            case 0: this.addon1 = "Tomato"; this. no += 1; this.price += 0.5; break;
            case 1: this.addon2 = "Tomato"; this. no += 1; this.price += 0.5; break;
            case 2: this.addon3 = "Tomato"; this. no += 1; this.price += 0.5; break;
            case 3: this.addon4 = "Tomato"; this. no += 1; this.price += 0.5; break;
        }
        return 1;
    }

    public int addCheese() {
        if(isFull() == 0) {
            System.out.println("No more addons! ");
            return 0;
        }
        switch(this.no) {
            case 0: this.addon1 = "Cheese"; this. no += 1; this.price += 1; break;
            case 1: this.addon2 = "Cheese"; this. no += 1; this.price += 1; break;
            case 2: this.addon3 = "Cheese"; this. no += 1; this.price += 1; break;
            case 3: this.addon4 = "Cheese"; this. no += 1; this.price += 1; break;
        }
        return 1;
    }

    public int addOnion() {
        if(isFull() == 0) {
            System.out.println("No more addons! ");
            return 0;
        }
        switch(this.no) {
            case 0: this.addon1 = "Onion"; this. no += 1; this.price += 0.3; break;
            case 1: this.addon2 = "Onion"; this. no += 1; this.price += 0.3; break;
            case 2: this.addon3 = "Onion"; this. no += 1; this.price += 0.3; break;
            case 3: this.addon4 = "Onion"; this. no += 1; this.price += 0.3; break;
        }
        return 1;
    }

    public void printBurger() {
        System.out.println("Your burger contains now: "
                        + "bread type: " + this.bread + ", meat type: " + this.meat);
        if( this.no == 0 ) System.out.println("And no additions. ");
        if( this.no == 1 ) System.out.println("And one addition: " + this.addon1);
        if( this.no == 2 ) System.out.println("And two additions: " + this.addon1 + ", " + this.addon2);
        if( this.no == 3 ) System.out.println("And three additions: " + this.addon1 + ", " + this.addon2 + ", " + this.addon3);
        if( this.no == 4 ) System.out.println("And four additions: " + this.addon1 + ", " + this.addon2 + ", "
                + this.addon3 + ", " + this.addon4);
        System.out.println("The price of burger is: $" + this.price);
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public double getPrice() {
        return price;
    }

    public int getNo() {
        return no;
    }

    public String getAddon1() {
        return addon1;
    }

    public String getAddon2() {
        return addon2;
    }

    public String getAddon3() {
        return addon3;
    }

    public String getAddon4() {
        return addon4;
    }
}
